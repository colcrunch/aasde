from django.db import models


# Create your models here.
class SDEVersion(models.Model):
    server_version = models.IntegerField(unique=True)
    sde_version = models.DateTimeField(null=False)

    def __str__(self):
        return "{} ({})".format(self.server_version, self.sde_version)


class agtAgents(models.Model):
    agentID = models.IntegerField(primary_key=True, unique=True)
    divisionID = models.IntegerField(null=True)
    corporationID = models.IntegerField(null=True)
    locationID = models.IntegerField(null=True)
    level = models.IntegerField(null=True)
    quality = models.IntegerField(null=True)
    agentTypeID = models.IntegerField(null=True)
    isLocator = models.NullBooleanField()


class agtAgentTypes(models.Model):
    agentTypeID = models.IntegerField(null=True)
    agentType = models.CharField(max_length=50)


class agtResearchAgents(models.Model):
    agentID = models.IntegerField(null=True)
    typeID = models.IntegerField(null=True)


class certCerts(models.Model):
    certID = models.IntegerField(primary_key=True, unique=True)
    description = models.TextField(null=True)
    groupID = models.IntegerField(null=True)
    name = models.CharField(max_length=255)


class certMasteries(models.Model):
    typeID = models.IntegerField(null=True)
    masteryLevel = models.IntegerField(null=True)
    certID = models.IntegerField(null=True)


class certSkills(models.Model):
    certID = models.IntegerField(null=True)
    skillID = models.IntegerField(null=True)
    certLevelInt = models.IntegerField(null=True)
    skillLevel = models.IntegerField(null=True)
    certLevelText = models.TextField(null=True)


class chrAncestries(models.Model):
    ancestryID = models.IntegerField(primary_key=True, unique=True)
    ancestryName = models.CharField(max_length=100)
    bloodlineID = models.IntegerField(null=True)
    description = models.CharField(max_length=1000)
    perception = models.IntegerField(null=True)
    willpower = models.IntegerField(null=True)
    charisma = models.IntegerField(null=True)
    memory = models.IntegerField(null=True)
    intelligence = models.IntegerField(null=True)
    iconID = models.IntegerField(null=True)
    shortDescription = models.CharField(max_length=500)


class chrAttributes(models.Model):
    attributeID = models.IntegerField(primary_key=True, unique=True)
    attributeName = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    iconID = models.IntegerField(null=True)
    shortDescription = models.CharField(max_length=500)
    notes = models.CharField(max_length=500)


class chrBloodlines(models.Model):
    bloodlineID = models.IntegerField(primary_key=True, unique=True)
    bloodlineName = models.CharField(max_length=100)
    raceID = models.IntegerField(null=True)
    description = models.CharField(max_length=1000)
    maleDescription = models.CharField(max_length=1000)
    femaleDescription = models.CharField(max_length=1000)
    shipTypeID = models.IntegerField(null=True)
    corporationID = models.IntegerField(null=True)
    perception = models.IntegerField(null=True)
    willpower = models.IntegerField(null=True)
    charisma = models.IntegerField(null=True)
    memory = models.IntegerField(null=True)
    intelligence = models.IntegerField(null=True)
    iconID = models.IntegerField(null=True)
    shortDescription = models.CharField(max_length=500)
    shortMaleDescription = models.CharField(max_length=500)
    shortFemaleDescription = models.CharField(max_length=500)


class chrFactions(models.Model):
    factionID = models.IntegerField(primary_key=True, unique=True)
    factionName = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    raceIDs = models.IntegerField(null=True)
    solarSystemID = models.IntegerField(null=True)
    corporationID = models.IntegerField(null=True)
    sizeFactor = models.FloatField(null=True)
    stationCount = models.IntegerField(null=True)
    stationSystemCount = models.IntegerField(null=True)
    militiaCorporationID = models.IntegerField(null=True)
    iconID = models.IntegerField(null=True)


class chrRaces(models.Model):
    raceID = models.IntegerField(primary_key=True, unique=True)
    raceName = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, null=True)
    iconID = models.IntegerField(null=True)
    shortDescription = models.CharField(max_length=500)


class crpActivities(models.Model):
    activityID = models.IntegerField(primary_key=True, unique=True)
    activityName = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)


class crpNPCCorporationDivisions(models.Model):
    corporationID = models.IntegerField(null=True)
    divisionID = models.IntegerField(null=True)
    size = models.IntegerField(null=True)

    class Meta:
        unique_together = (("corporationID", "divisionID"),)


class crpNPCCorporationResearchFields(models.Model):
    skillID = models.IntegerField(null=True)
    corporationID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("skillID", "corporationID"),)


class crpNPCCorporations(models.Model):
    corporationID = models.IntegerField(primary_key=True, unique=True)
    size = models.CharField(max_length=1)
    extent = models.CharField(max_length=1)
    solarSystemID = models.IntegerField(null=True)
    investorID1 = models.IntegerField(null=True)
    investorShares1 = models.IntegerField(null=True)
    investorID2 = models.IntegerField(null=True)
    investorShares2 = models.IntegerField(null=True)
    investorID3 = models.IntegerField(null=True)
    investorShares3 = models.IntegerField(null=True)
    investorID4 = models.IntegerField(null=True)
    investorShares4 = models.IntegerField(null=True)
    friendID = models.IntegerField(null=True)
    enemyID = models.IntegerField(null=True)
    publicShares = models.IntegerField(null=True)
    initialPrice = models.IntegerField(null=True)
    minSecurity = models.FloatField(null=True)
    scattered = models.NullBooleanField()
    fringe = models.IntegerField(null=True)
    corridor = models.IntegerField(null=True)
    hub = models.IntegerField(null=True)
    border = models.IntegerField(null=True)
    factionID = models.IntegerField(null=True)
    sizeFactor = models.FloatField(null=True)
    stationCount = models.IntegerField(null=True)
    stationSystemCount = models.IntegerField(null=True)
    description = models.CharField(max_length=4000)
    iconID = models.IntegerField(null=True)


class crpNPCCorporationTrades(models.Model):
    corporationID = models.IntegerField(null=True)
    typeID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("corporationID", "typeID"),)


class crpNPCDivisions(models.Model):
    divisionID = models.IntegerField(primary_key=True, unique=True)
    divisionName = models.CharField(null=True, max_length=100)
    description = models.CharField(null=True, max_length=1000)
    leaderType = models.CharField(null=True, max_length=100)


class dgmAttributeCategories(models.Model):
    categoryID = models.IntegerField(null=True)
    categoryName = models.CharField(null=True, max_length=50)
    categoryDescription = models.CharField(null=True, max_length=200)


class dgmAttributeTypes(models.Model):
    attributeID = models.IntegerField(primary_key=True, unique=True)
    attributeName = models.CharField(null=True, max_length=100)
    description = models.CharField(null=True, max_length=1000)
    iconID = models.IntegerField(null=True)
    defaultValue = models.FloatField(null=True)
    published = models.NullBooleanField()
    displayName = models.CharField(max_length=150, null=True)
    unitID = models.IntegerField(null=True)
    stackable = models.NullBooleanField()
    highIsGood = models.NullBooleanField()
    categoryID = models.IntegerField(null=True)


class dgmEffects(models.Model):
    effectID = models.IntegerField(primary_key=True, unique=True)
    effectName = models.CharField(null=True, max_length=400)
    effectCategory = models.IntegerField(null=True)
    preExpression = models.IntegerField(null=True)
    postExpression = models.IntegerField(null=True)
    description = models.CharField(null=True, max_length=1000)
    guid = models.CharField(null=True, max_length=60)
    iconID = models.IntegerField(null=True)
    isOffensive = models.NullBooleanField()
    isAssistance = models.NullBooleanField()
    durationAttributeID = models.IntegerField(null=True)
    trackingSpeedAttributeID = models.IntegerField(null=True)
    dischargeAttributeID = models.IntegerField(null=True)
    rangeAttributeID = models.IntegerField(null=True)
    falloffAttributeID = models.IntegerField(null=True)
    disallowAutoRepeat = models.NullBooleanField()
    published = models.NullBooleanField()
    displayName = models.CharField(max_length=100, null=True)
    isWarpSafe = models.NullBooleanField()
    rangeChance = models.NullBooleanField()
    electronicChance = models.NullBooleanField()
    propulsionChance = models.NullBooleanField()
    distribution = models.IntegerField(null=True)
    sfxName = models.CharField(null=True, max_length=20)
    npcUsageChanceAttributeID = models.IntegerField(null=True)
    npcActivationChanceAttributeID = models.IntegerField(null=True)
    fittingUsageChanceAttributeID = models.IntegerField(null=True)
    modifierInfo = models.TextField(null=True)


class dgmExpressions(models.Model):
    expressionID = models.IntegerField(primary_key=True, unique=True)
    operandID = models.IntegerField(null=True)
    arg1 = models.IntegerField(null=True)
    arg2 = models.IntegerField(null=True)
    expressionValue = models.CharField(null=True, max_length=100)
    description = models.CharField(null=True, max_length=1000)
    expressionName = models.CharField(null=True, max_length=500)
    expressionTypeID = models.IntegerField(null=True)
    expressionGroupID = models.IntegerField(null=True)
    expressionAttributeID = models.IntegerField(null=True)


class dgmTypeAttributes(models.Model):
    typeID = models.IntegerField(null=True)
    attributeID = models.IntegerField(null=True)
    valueInt = models.IntegerField(null=True)
    valueFloat = models.FloatField(null=True)

    class Meta:
        unique_together = (("typeID", "attributeID"),)


class dgmTypeEffects(models.Model):
    typeID = models.IntegerField(null=True)
    effectID = models.IntegerField(null=True)
    isDefault = models.NullBooleanField()

    class Meta:
        unique_together = (("typeID", "effectID"),)


class eveGraphics(models.Model):
    graphicID = models.IntegerField(primary_key=True, unique=True)
    sofFactionName = models.CharField(null=True, max_length=100)
    graphicFile = models.CharField(null=True, max_length=100)
    sofHullName = models.CharField(null=True, max_length=100)
    sofRaceName = models.CharField(null=True, max_length=100)
    description = models.TextField(null=True)


class eveIcons(models.Model):
    iconID = models.IntegerField(primary_key=True, unique=True)
    iconFile = models.CharField(null=True, max_length=500)
    description = models.TextField(null=True)


class eveUnits(models.Model):
    unitID = models.IntegerField(primary_key=True, unique=True)
    unitName = models.CharField(null=True, max_length=100)
    displayName = models.CharField(max_length=50, null=True)
    description = models.CharField(max_length=1000, null=True)


class industryActivity(models.Model):
    typeID = models.IntegerField(null=True)
    activityID = models.IntegerField(null=True)
    time = models.IntegerField(null=True)

    class Meta:
        unique_together = (("typeID", "activityID"),)


class industryActivityMaterials(models.Model):
    typeID = models.IntegerField(null=True)
    activityID = models.IntegerField(null=True)
    materialTypeID = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)


class industryActivityProbabilities(models.Model):
    typeID = models.IntegerField(null=True)
    activityID = models.IntegerField(null=True)
    productTypeID = models.IntegerField(null=True)
    probability = models.DecimalField(max_digits=3, decimal_places=2)


class industryActivityProducts(models.Model):
    typeID = models.IntegerField(null=True)
    activityID = models.IntegerField(null=True)
    productTypeID = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)


class industryActivityRaces(models.Model):
    typeID = models.IntegerField(null=True)
    activityID = models.IntegerField(null=True)
    productTypeID = models.IntegerField(null=True)
    raceID = models.IntegerField(null=True)


class industryActivitySkills(models.Model):
    typeID = models.IntegerField(null=True)
    activityID = models.IntegerField(null=True)
    skillID = models.IntegerField(null=True)
    level = models.IntegerField(null=True)


class industryBlueprints(models.Model):
    typeID = models.IntegerField(primary_key=True, unique=True)
    maxProductionLimit = models.IntegerField(null=True)


class invCategories(models.Model):
    categoryID = models.IntegerField(primary_key=True, unique=True)
    categoryName = models.CharField(null=True, max_length=100)
    iconID = models.IntegerField(null=True)
    published = models.NullBooleanField()


class invContrabandTypes(models.Model):
    factionID = models.IntegerField(null=True)
    typeID = models.IntegerField(null=True)
    standingLoss = models.FloatField(null=True)
    confiscateMinSec = models.FloatField(null=True)
    fineByValue = models.FloatField(null=True)
    attackMinSec = models.FloatField(null=True)

    class Meta:
        unique_together = (("factionID", "typeID"),)


class invControlTowerResourcePurposes(models.Model):
    purpose = models.IntegerField(null=True)
    purposeText = models.CharField(null=True, max_length=100)


class invControlTowerResources(models.Model):
    controlTowerTypeID = models.IntegerField(null=True)
    resourceTypeID = models.IntegerField(null=True)
    purpose = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)
    minSecurityLevel = models.FloatField(null=True)
    factionID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("controlTowerTypeID", "resourceTypeID"),)


class invFlags(models.Model):
    flagID = models.IntegerField(primary_key=True, unique=True)
    flagName = models.CharField(null=True, max_length=200)
    flagText = models.CharField(null=True, max_length=100)
    orderID = models.IntegerField(null=True)


class invGroups(models.Model):
    groupID = models.IntegerField(primary_key=True, unique=True)
    categoryID = models.IntegerField(null=True)
    groupName = models.CharField(null=True, max_length=100)
    iconID = models.IntegerField(null=True)
    useBasePrice = models.NullBooleanField()
    anchored = models.NullBooleanField()
    anchorable = models.NullBooleanField()
    fittableNonSingleton = models.NullBooleanField()
    published = models.NullBooleanField()


class invItems(models.Model):
    itemID = models.IntegerField(primary_key=True, unique=True)
    typeID = models.IntegerField(null=True)
    ownerID = models.IntegerField(null=True)
    locationID = models.IntegerField(null=True)
    flagID = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)


class invMarketGroups(models.Model):
    marketGroupID = models.IntegerField(primary_key=True, unique=True)
    parentGroupID = models.IntegerField(null=True)
    marketGroupName = models.CharField(max_length=100)
    description = models.CharField(null=True, max_length=3000)
    iconID = models.IntegerField(null=True)
    hasTypes = models.NullBooleanField()


class invMetaGroups(models.Model):
    metaGroupID = models.IntegerField(primary_key=True, unique=True)
    metaGroupName = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, null=True)
    iconID = models.IntegerField(null=True)


class invMetaTypes(models.Model):
    typeID = models.IntegerField(primary_key=True, unique=True)
    parentTypeID = models.IntegerField(null=True)
    metaGroupID = models.IntegerField(null=True)


class invNames(models.Model):
    itemID = models.IntegerField(primary_key=True, unique=True)
    itemName = models.CharField(max_length=200)


class invPositions(models.Model):
    itemID = models.IntegerField(primary_key=True, unique=True)
    x = models.FloatField(null=True)
    y = models.FloatField(null=True)
    z = models.FloatField(null=True)
    yaw = models.FloatField(null=True)
    pitch = models.FloatField(null=True)
    roll = models.FloatField(null=True)


class invTraits(models.Model):
    traitID = models.IntegerField(primary_key=True, unique=True)
    typeID = models.IntegerField(null=True)
    skillID = models.IntegerField(null=True)
    bonus = models.FloatField(null=True)
    bonusText = models.TextField(null=True)
    unitID = models.IntegerField(null=True)


class invTypeMaterials(models.Model):
    typeID = models.IntegerField(null=True)
    materialTypeID = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)

    class Meta:
        unique_together = (("typeID", "materialTypeID"),)


class invTypeReactions(models.Model):
    reactionTypeID = models.IntegerField(null=True)
    input = models.NullBooleanField()
    typeID = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)

    class Meta:
        unique_together = (("reactionTypeID", "input", "typeID"),)


class invTypes(models.Model):
    typeID = models.IntegerField(primary_key=True, unique=True)
    groupID = models.IntegerField(null=True)
    typeName = models.CharField(null=True, max_length=100)
    description = models.TextField(null=True)
    mass = models.FloatField(null=True)
    volume = models.FloatField(null=True)
    capacity = models.FloatField(null=True)
    portionSize = models.IntegerField(null=True)
    raceID = models.IntegerField(null=True)
    basePrice = models.DecimalField(null=True, max_digits=19, decimal_places=4)
    published = models.NullBooleanField()
    marketGroupID = models.IntegerField(null=True)
    iconID = models.IntegerField(null=True)
    soundID = models.IntegerField(null=True)
    graphicID = models.IntegerField(null=True)


class invUniqueNames(models.Model):
    itemID = models.IntegerField(primary_key=True, unique=True)
    itemName = models.CharField(max_length=200, unique=True)
    groupID = models.IntegerField(null=True)


class invVolumes(models.Model):
    typeID = models.IntegerField(primary_key=True, unique=True)
    volume = models.IntegerField(null=True)


class mapCelestialStatistics(models.Model):
    celestialID = models.IntegerField(primary_key=True, unique=True)
    temperature = models.FloatField(null=True)
    spectralClass = models.CharField(null=True, max_length=10)
    luminosity = models.FloatField(null=True)
    age = models.FloatField(null=True)
    life = models.FloatField(null=True)
    orbitRadius = models.FloatField(null=True)
    eccentricity = models.FloatField(null=True)
    massDust = models.FloatField(null=True)
    massGas = models.FloatField(null=True)
    fragmented = models.NullBooleanField()
    density = models.FloatField(null=True)
    surfaceGravity = models.FloatField(null=True)
    escapeVelocity = models.FloatField(null=True)
    orbitPeriod = models.FloatField(null=True)
    rotationRate = models.FloatField(null=True)
    locked = models.NullBooleanField()
    pressure = models.FloatField(null=True)
    radius = models.FloatField(null=True)
    mass = models.IntegerField(null=True)


class mapConstellationJumps(models.Model):
    fromRegionID = models.IntegerField(null=True)
    fromConstellationID = models.IntegerField(null=True)
    toConstellationID = models.IntegerField(null=True)
    toRegionID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("fromConstellationID", "toConstellationID"),)


class mapConstellations(models.Model):
    constellationName = models.CharField(max_length=500)
    regionID = models.IntegerField(null=True)
    constellationID = models.IntegerField(primary_key=True, unique=True)
    x = models.FloatField(null=True)
    y = models.FloatField(null=True)
    z = models.FloatField(null=True)
    xMin = models.FloatField(null=True)
    yMin = models.FloatField(null=True)
    zMin = models.FloatField(null=True)
    xMax = models.FloatField(null=True)
    yMax = models.FloatField(null=True)
    zMax = models.FloatField(null=True)
    factionID = models.IntegerField(null=True)
    radius = models.FloatField(null=True)


class mapDenormalize(models.Model):
    itemID = models.IntegerField(primary_key=True, unique=True)
    typeID = models.IntegerField(null=True)
    groupID = models.IntegerField(null=True)
    solarSystemID = models.IntegerField(null=True)
    constellationID = models.IntegerField(null=True)
    regionID = models.IntegerField(null=True)
    orbitID = models.IntegerField(null=True)
    x = models.FloatField(null=True)
    y = models.FloatField(null=True)
    z = models.FloatField(null=True)
    radius = models.FloatField(null=True)
    itemName = models.CharField(max_length=100, null=True)
    security = models.FloatField(null=True)
    celestialIndex = models.IntegerField(null=True)
    orbitIndex = models.IntegerField(null=True)


class mapJumps(models.Model):
    stargateID = models.IntegerField(primary_key=True, unique=True)
    destinationID = models.IntegerField(null=True)


class mapLandmarks(models.Model):
    landmarkID = models.IntegerField(primary_key=True, unique=True)
    landmarkName = models.CharField(max_length=100)
    description = models.TextField(null=True)
    locationID = models.IntegerField(null=True)
    x = models.FloatField(null=True)
    y = models.FloatField(null=True)
    z = models.FloatField(null=True)
    iconID = models.IntegerField(null=True)


class mapLocationScenes(models.Model):
    locationID = models.IntegerField(primary_key=True, unique=True)
    graphicID = models.IntegerField(null=True)


class mapLocationWormholeClasses(models.Model):
    locationID = models.IntegerField(primary_key=True, unique=True)
    wormholeClassID = models.IntegerField(null=True)


class mapRegionJumps(models.Model):
    fromRegionID = models.IntegerField(null=True)
    toRegionID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("fromRegionID", "toRegionID"),)


class mapRegions(models.Model):
    regionID = models.IntegerField(primary_key=True, unique=True)
    regionName = models.CharField(max_length=100)
    x = models.FloatField(null=True)
    y = models.FloatField(null=True)
    z = models.FloatField(null=True)
    xMin = models.FloatField(null=True)
    yMin = models.FloatField(null=True)
    zMin = models.FloatField(null=True)
    xMax = models.FloatField(null=True)
    yMax = models.FloatField(null=True)
    zMax = models.FloatField(null=True)
    factionID = models.IntegerField(null=True)
    radius = models.FloatField(null=True)


class mapSolarSystemJumps(models.Model):
    fromRegionID = models.IntegerField(null=True)
    fromConstellationID = models.IntegerField(null=True)
    fromSolarSystemID = models.IntegerField(null=True)
    toSolarSystemID = models.IntegerField(null=True)
    toConstellationID = models.IntegerField(null=True)
    toRegionID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("fromSolarSystemID", "toSolarSystemID"),)


class mapSolarSystems(models.Model):
    regionID = models.IntegerField(null=True)
    constellationID = models.IntegerField(null=True)
    solarSystemID = models.IntegerField(primary_key=True, unique=True)
    solarSystemName = models.CharField(max_length=100)
    x = models.FloatField(null=True)
    y = models.FloatField(null=True)
    z = models.FloatField(null=True)
    xMin = models.FloatField(null=True)
    yMin = models.FloatField(null=True)
    zMin = models.FloatField(null=True)
    xMax = models.FloatField(null=True)
    yMax = models.FloatField(null=True)
    zMax = models.FloatField(null=True)
    luminosity = models.FloatField(null=True)
    border = models.NullBooleanField()
    fringe = models.NullBooleanField()
    corridor = models.NullBooleanField()
    hub = models.NullBooleanField()
    international = models.NullBooleanField()
    regional = models.NullBooleanField()
    constellation = models.NullBooleanField()
    security = models.FloatField(null=True)
    factionID = models.IntegerField(null=True)
    radius = models.FloatField(null=True)
    sunTypeID = models.IntegerField(null=True)
    securityClass = models.CharField(max_length=2, null=True)


class mapUniverse(models.Model):
    universeID = models.IntegerField(primary_key=True, unique=True)
    universeName = models.CharField(max_length=100)
    x = models.FloatField(null=True)
    y = models.FloatField(null=True)
    z = models.FloatField(null=True)
    xMin = models.FloatField(null=True)
    yMin = models.FloatField(null=True)
    zMin = models.FloatField(null=True)
    xMax = models.FloatField(null=True)
    yMax = models.FloatField(null=True)
    zMax = models.FloatField(null=True)
    radius = models.FloatField(null=True)


class planetSchematics(models.Model):
    schematicID = models.IntegerField(primary_key=True, unique=True)
    schematicName = models.CharField(max_length=255)
    cycleTime = models.IntegerField(null=True)


class planetSchematicsPinMap(models.Model):
    schematicID = models.IntegerField(null=True)
    pinTypeID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("schematicID", "pinTypeID"),)


class planetSchematicsTypeMap(models.Model):
    schematicID = models.IntegerField(null=True)
    typeID = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)
    isInput = models.NullBooleanField()

    class Meta:
        unique_together = (("schematicID", "typeID"),)


class ramActivities(models.Model):
    activityID = models.IntegerField(primary_key=True, unique=True)
    activityName = models.CharField(max_length=100)
    iconNo = models.CharField(max_length=5, null=True)
    description = models.CharField(max_length=1000)
    published = models.NullBooleanField()


class ramAssemblyLineStations(models.Model):
    stationID = models.IntegerField(null=True)
    assemblyLineTypeID = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)
    stationTypeID = models.IntegerField(null=True)
    ownerID = models.IntegerField(null=True)
    solarSystemID = models.IntegerField(null=True)
    regionID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("stationID", "assemblyLineTypeID"),)


class ramAssemblyLineTypeDetailPerCategory(models.Model):
    assemblyLineTypeID = models.IntegerField(null=True)
    categoryID = models.IntegerField(null=True)
    timeMultiplier = models.FloatField(null=True)
    materialMultiplier = models.FloatField(null=True)
    costMultiplier = models.FloatField(null=True)

    class Meta:
        unique_together = (("categoryID", "assemblyLineTypeID"),)


class ramAssemblyLineTypeDetailPerGroup(models.Model):
    assemblyLineTypeID = models.IntegerField(null=True)
    groupID = models.IntegerField(null=True)
    timeMultiplier = models.FloatField(null=True)
    materialMultiplier = models.FloatField(null=True)
    costMultiplier = models.FloatField(null=True)

    class Meta:
        unique_together = (("assemblyLineTypeID", "groupID"),)


class ramAssemblyLineTypes(models.Model):
    assemblyLineTypeID = models.IntegerField(primary_key=True, unique=True)
    assemblyLineTypeName = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    baseTimeMultiplier = models.FloatField(null=True)
    baseMaterialMultiplier = models.FloatField(null=True)
    baseCostMultiplier = models.FloatField(null=True)
    volume = models.FloatField(null=True)
    activityID = models.IntegerField(null=True)
    minCostPerHour = models.FloatField(null=True)


class ramInstallationTypeContents(models.Model):
    installationTypeID = models.IntegerField(null=True)
    assemblyLineTypeID = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)

    class Meta:
        unique_together = (("installationTypeID", "assemblyLineTypeID"),)


class skinLicense(models.Model):
    licenseTypeID = models.IntegerField(primary_key=True, unique=True)
    duration = models.IntegerField(null=True)
    skinID = models.IntegerField(null=True)


class skinMaterials(models.Model):
    skinMaterialID = models.IntegerField(primary_key=True, unique=True)
    displayNameID = models.IntegerField(null=True)
    materialSetID = models.IntegerField(null=True)


class skins(models.Model):
    skinID = models.IntegerField(primary_key=True, unique=True)
    internalName = models.CharField(max_length=70)
    skinMaterialID = models.IntegerField(null=True)


class skinShip(models.Model):
    skinID = models.IntegerField(null=True)
    typeID = models.IntegerField(null=True)


class staOperations(models.Model):
    activityID = models.IntegerField(null=True)
    operationID = models.IntegerField(null=True)
    operationName = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, null=True)
    fringe = models.IntegerField(null=True)
    corridor = models.IntegerField(null=True)
    hub = models.IntegerField(null=True)
    border = models.IntegerField(null=True)
    ratio = models.IntegerField(null=True)
    caldariStationTypeID = models.IntegerField(null=True)
    minmatarStationTypeID = models.IntegerField(null=True)
    amarrStationTypeID = models.IntegerField(null=True)
    gallenteStationTypeID = models.IntegerField(null=True)
    joveStationTypeID = models.IntegerField(null=True)


class staOperationsServices(models.Model):
    operationID = models.IntegerField(null=True)
    serviceID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("operationID", "serviceID"),)


class staServices(models.Model):
    serviceID = models.IntegerField(primary_key=True, unique=True)
    serviceName = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, null=True)


class staStations(models.Model):
    stationID = models.IntegerField(primary_key=True, unique=True)
    security = models.FloatField(null=True)
    dockingCostPerVolume = models.FloatField(null=True)
    maxShipVolumeDockable = models.FloatField(null=True)
    officeRentalCost = models.IntegerField(null=True)
    operationID = models.IntegerField(null=True)
    stationTypeID = models.IntegerField(null=True)
    corporationID = models.IntegerField(null=True)
    solarSystemID = models.IntegerField(null=True)
    constellationID = models.IntegerField(null=True)
    regionID = models.IntegerField(null=True)
    stationName = models.CharField(max_length=100)
    x = models.FloatField(null=True)
    y = models.FloatField(null=True)
    z = models.FloatField(null=True)
    reprocessingEfficiency = models.FloatField(null=True)
    reprocessingStationsTake = models.FloatField(null=True)
    reprocessingHangarFlag = models.IntegerField(null=True)


class staStationTypes(models.Model):
    stationTypeID = models.IntegerField(primary_key=True, unique=True)
    dockEntryX = models.FloatField(null=True)
    dockEntryY = models.FloatField(null=True)
    dockEntryZ = models.FloatField(null=True)
    dockOrientationX = models.FloatField(null=True)
    dockOrientationY = models.FloatField(null=True)
    dockOrientationZ = models.FloatField(null=True)
    operationID = models.IntegerField(null=True)
    officeSlots = models.IntegerField(null=True)
    reprocessingEfficiency = models.FloatField(null=True)
    conquerable = models.NullBooleanField()


class translationTables(models.Model):
    sourceTable = models.CharField(max_length=200)
    destinationTable = models.CharField(max_length=200)
    translatedKey = models.CharField(max_length=200)
    tcGroupID = models.IntegerField(null=True)
    tcID = models.IntegerField(null=True)

    class Meta:
        unique_together = (("sourceTable", "translatedKey"),)


class trnTranslationColumns(models.Model):
    tcGroupID = models.IntegerField(null=True)
    tcID = models.IntegerField(null=True)
    tableName = models.CharField(max_length=256)
    columnName = models.CharField(max_length=128)
    masterID = models.CharField(max_length=128)


class trnTranslationLanguages(models.Model):
    numericLanguageID = models.IntegerField(primary_key=True, unique=True)
    languageID = models.CharField(max_length=50)
    languageName = models.CharField(max_length=200)


class trnTranslations(models.Model):
    tcID = models.IntegerField(null=True)
    keyID = models.IntegerField(null=True)
    languageID = models.CharField(max_length=5, null=True)
    text = models.TextField(null=True)

    class Meta:
        unique_together = (("tcID", "keyID", "languageID"),)


class warCombatZones(models.Model):
    combatZoneID = models.IntegerField(primary_key=True, unique=True)
    combatZoneName = models.CharField(max_length=100)
    factionID = models.IntegerField(null=True)
    centerSystemID = models.IntegerField(null=True)
    description = models.CharField(max_length=500)


class warCombatZoneSystems(models.Model):
    solarSystemID = models.IntegerField(primary_key=True, unique=True)
    combatZoneID = models.IntegerField(null=True)
