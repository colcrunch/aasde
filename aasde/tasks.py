import logging
from celery import shared_task
from .models import SDEVersion, mapSolarSystems, agtAgents, agtAgentTypes, agtResearchAgents, certCerts, \
    certMasteries, certSkills, chrAncestries, chrAttributes, chrBloodlines, chrFactions, chrRaces, crpActivities, \
    crpNPCCorporationDivisions, crpNPCCorporationResearchFields, crpNPCCorporations, crpNPCCorporationTrades, \
    crpNPCDivisions, dgmAttributeCategories, dgmAttributeTypes, dgmEffects, dgmExpressions, dgmTypeAttributes, \
    dgmTypeEffects, eveGraphics, eveIcons, eveUnits, industryActivity, industryActivityMaterials, \
    industryActivityProbabilities, industryActivityProducts, industryActivityRaces, industryActivitySkills, \
    industryBlueprints, invCategories, invContrabandTypes, invControlTowerResourcePurposes, invControlTowerResources, \
    invFlags, invGroups, invItems, invMarketGroups, invMetaGroups, invMetaTypes, invNames, invPositions, invTraits, \
    invTypeMaterials, invTypeReactions, invTypes, invUniqueNames, invVolumes, mapCelestialStatistics, \
    mapConstellationJumps, mapConstellations, mapDenormalize, mapJumps, mapLandmarks, mapLocationScenes, \
    mapLocationWormholeClasses, mapRegionJumps, mapRegions, mapSolarSystemJumps, mapUniverse, planetSchematics, \
    planetSchematicsPinMap, planetSchematicsTypeMap, ramActivities, ramAssemblyLineStations, \
    ramAssemblyLineTypeDetailPerCategory, ramAssemblyLineTypeDetailPerGroup, ramAssemblyLineTypes, \
    ramInstallationTypeContents, skinLicense, skinMaterials, skins, skinShip, staOperations, staOperationsServices, \
    staServices, staStations, staStationTypes, translationTables, trnTranslationColumns, trnTranslationLanguages, \
    trnTranslations, warCombatZones, warCombatZoneSystems
from esi.clients import esi_client_factory
from html.parser import HTMLParser
from django.utils import timezone
import bz2
import sqlite3
import requests
import datetime

logger = logging.getLogger(__name__)


class FuzzyDates(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.record_data = False
        self.f_date = None

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            if attrs == [('href', 'eve.db.bz2')]:
                # Get the Text Here
                self.record_data = True
            elif attrs[0][1] != 'eve.db.bz2':
                self.record_data = False

    def handle_data(self, data):
        if self.record_data is True:
            self.f_date = data


class SDEConn:
    def __init__(self):
        self.conn = sqlite3.connect('eve.db', detect_types=sqlite3.PARSE_COLNAMES)
        self.cursor = self.conn.cursor()
        self.col_names = None

    def __get_col_names(self):
        return list(map(lambda x: x[0], self.cursor.description))

    def execute_all(self, stmt):
        c = self.cursor
        t = c.execute(stmt)
        self.col_names = self.__get_col_names()
        all = t.fetchall()
        named_result = []
        for row in all:
            named = {}
            for idx, col in enumerate(self.col_names):
                named[col] = row[idx]
            named_result.append(named)
        self.close()
        return named_result

    def execute_one(self, stmt):
        c = self.cursor
        t = c.execute(stmt)
        self.col_names = self.__get_col_names()
        one = t.fetchone()
        named_result = {}
        for idx, col in enumerate(self.col_names):
            named_result[col] = one[idx]
        self.close()
        return named_result

    def close(self):
        return self.conn.close()


@shared_task
def check_version():
    """
    Checks what the last version of the SDE recorded was.
    :return: This task does not return antything.
    """
    logger.debug("SDE Version Check started.")
    # Get the TQ version and latest date from Fuzzworks.

    client = esi_client_factory(version='latest')
    version = client.Status.get_status().result()

    url = 'https://www.fuzzwork.co.uk/dump/latest/?C=M;O=A'
    r = requests.get(url)

    parser = FuzzyDates()
    parser.feed(r.text)
    fuzzy_data = parser.f_date.strip()
    parser.close()

    date_string = fuzzy_data[:16]
    date_time = datetime.datetime.strptime(date_string, '%Y-%m-%d %H:%M')

    try:
        # Check version currently in use
        current_version = SDEVersion.objects.all().order_by('-server_version')[0]
        logger.debug("Current SDE Version: {}" .format(current_version.__str__))
    except IndexError:
        # This should only happen on first run.
        logger.debug("First run! Spawning download task for the first time!")
        sde_ver = SDEVersion(server_version=int(version['server_version']), sde_version=date_time.replace(tzinfo=timezone.utc))
        sde_ver.save()
        download_sde.delay()
        return  # Just to make sure that we dont continue with this function

    # Check for a TQ version update.
    # (It is extremely unlikely that we will need to perform an update if the TQ version has not changed)
    if int(version['server_version']) == current_version.server_version:
        logger.debug("No Update needed. (Determined based on TQ Version)")
        return  # In this case we do not need to do anything.

    # We dont always need a new SDE version just cause the TQ version changed. To determine this, we will compare the
    # date on the last downloaded version to the latest version on Fuzzworks.

    if date_time == current_version.sde_version:
        logger.debug("No Update needed. (Determined based on Fussworks export timeztamp)")
        return  # No need for an update

    logger.debug("Looks like there is a new SDE version out there. Spawning download task!")
    sde_ver = SDEVersion(server_version=version['server_version'], sde_version=date_time.replace(tzinfo=timezone.utc))
    sde_ver.save()
    download_sde.delay()
    return


@shared_task
def download_sde():
    """
    This task downloads the Static Data Export from fuzzworks and unpacks it.
    :return:
    """
    logger.debug("SDE Download Task Started.")
    url = 'https://fuzzwork.co.uk/dump/latest/eve.db.bz2'
    r = requests.get(url)

    with open('eve.db.bz2', 'wb') as db:
        db.write(r.content)

    open('eve.db', 'wb').write(bz2.open('eve.db.bz2', 'rb').read())

    logger.debug("SDE downloaded and extracted. Spawning model specific tasks.")
    # Schedule all the model updates.
    Agt().all()
    Cert().all()
    Chr().all()
    Crp().all()
    Dgm().all()
    Eve().all()
    Industry().all()
    Inv().all()
    Map().all()
    Planet().all()
    Ram().all()
    Skin().all()
    Sta().all()
    Trn().all()
    War().all()
    return


class Agt:
    def all(self):
        self.agents.delay(0)
        self.agent_types.delay(0)
        self.agt_research_agents.delay(0)
        return

    @staticmethod
    @shared_task
    def agents(a):
        agtAgents.objects.all().delete()

        sde = SDEConn()
        agents = sde.execute_all("SELECT * FROM agtAgents")
        objs = [agtAgents(**agent) for agent in agents]
        agtAgents.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def agent_types(a):
        agtAgentTypes.objects.all().delete()
        sde = SDEConn()
        agtTypes = sde.execute_all("SELECT * FROM agtAgentTypes")
        objs = [agtAgentTypes(**agt) for agt in agtTypes]
        agtAgentTypes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def agt_research_agents(a):
        agtResearchAgents.objects.all().delete()
        ragents = SDEConn().execute_all("SELECT * FROM agtResearchAgents")
        objs = [agtResearchAgents(**ragent) for ragent in ragents]
        agtResearchAgents.objects.bulk_create(objs, batch_size=500)


class Cert:
    def all(self):
        self.certs.delay(0)
        self.masteries.delay(0)
        self.skills.delay(0)
        return

    @staticmethod
    @shared_task
    def certs(s):
        certCerts.objects.all().delete()
        certs = SDEConn().execute_all("SELECT * FROM certCerts")
        objs = [certCerts(**cert) for cert in certs]
        certCerts.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def masteries(s):
        certMasteries.objects.all().delete()
        masteries = SDEConn().execute_all("SELECT * FROM certMasteries")
        objs = [certMasteries(**mast) for mast in masteries]
        certMasteries.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def skills(s):
        certSkills.objects.all().delete()
        skills = SDEConn().execute_all("SELECT * FROM certSkills")
        objs = [certSkills(**skill) for skill in skills]
        certSkills.objects.bulk_create(objs, batch_size=500)


class Chr:
    def all(self):
        self.ancestries.delay(0)
        self.attributes.delay(0)
        self.bloodlines.delay(0)
        self.factions.delay(0)
        self.races.delay(0)
        return

    @staticmethod
    @shared_task
    def ancestries(s):
        chrAncestries.objects.all().delete()
        ancs = SDEConn().execute_all("SELECT * FROM chrAncestries")
        objs = [chrAncestries(**anc) for anc in ancs]
        chrAncestries.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def attributes(s):
        chrAttributes.objects.all().delete()
        atts = SDEConn().execute_all("SELECT * FROM chrAttributes")
        objs = [chrAttributes(**att) for att in atts]
        chrAttributes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def bloodlines(s):
        chrBloodlines.objects.all().delete()
        lines = SDEConn().execute_all("SELECT * FROM chrBloodlines")
        objs = [chrBloodlines(**line) for line in lines]
        chrBloodlines.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def factions(s):
        chrFactions.objects.all().delete()
        factions = SDEConn().execute_all("SELECT * FROM chrFactions")
        objs = [chrFactions(**faction) for faction in factions]
        chrFactions.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def races(s):
        chrRaces.objects.all().delete()
        races = SDEConn().execute_all("SELECT * FROM chrRaces")
        objs = [chrRaces(**race) for race in races]
        chrRaces.objects.bulk_create(objs, batch_size=500)


class Crp:
    def all(self):
        self.activities.delay(0)
        self.npc_corporation_divisions.delay(0)
        self.npc_corporation_research_fields.delay(0)
        self.npc_corporations.delay(0)
        self.npc_corporation_trades.delay(0)
        self.npc_divisions.delay(0)
        return

    @staticmethod
    @shared_task
    def activities(self):
        crpActivities.objects.all().delete()
        acts = SDEConn().execute_all("SELECT * FROM crpActivities")
        objs = [crpActivities(**act) for act in acts]
        crpActivities.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def npc_corporation_divisions(self):
        crpNPCCorporationDivisions.objects.all().delete()
        ncds = SDEConn().execute_all("SELECT * FROM crpNPCCorporationDivisions")
        objs = [crpNPCCorporationDivisions(**ncd) for ncd in ncds]
        crpNPCCorporationDivisions.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def npc_corporation_research_fields(self):
        crpNPCCorporationResearchFields.objects.all().delete()
        ncrfs = SDEConn().execute_all("SELECT * FROM crpNPCCorporationResearchFields")
        objs = [crpNPCCorporationResearchFields(**ncrf) for ncrf in ncrfs]
        crpNPCCorporationResearchFields.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def npc_corporations(self):
        crpNPCCorporations.objects.all().delete()
        ncs = SDEConn().execute_all("SELECT * FROM crpNPCCorporations")
        objs = [crpNPCCorporations(**nc) for nc in ncs]
        crpNPCCorporations.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def npc_corporation_trades(self):
        crpNPCCorporationTrades.objects.all().delete()
        ncts = SDEConn().execute_all("SELECT * FROM crpNPCCorporationTrades")
        objs = [crpNPCCorporationTrades(**nct) for nct in ncts]
        crpNPCCorporationTrades.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def npc_divisions(self):
        crpNPCDivisions.objects.all().delete()
        nds = SDEConn().execute_all("SELECT * FROM crpNPCDivisions")
        objs = [crpNPCDivisions(**nd) for nd in nds]
        crpNPCDivisions.objects.bulk_create(objs, batch_size=500)


class Dgm:
    def all(self):
        self.attribute_categories.delay(0)
        self.attribute_types.delay(0)
        self.effects.delay(0)
        self.expressions.delay(0)
        self.type_attributes.delay(0)
        self.type_effects.delay(0)
        return

    @staticmethod
    @shared_task
    def attribute_categories(self):
        dgmAttributeCategories.objects.all().delete()
        acs = SDEConn().execute_all("SELECT * FROM dgmAttributeCategories")
        objs = [dgmAttributeCategories(**ac) for ac in acs]
        dgmAttributeCategories.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def attribute_types(self):
        dgmAttributeTypes.objects.all().delete()
        ats = SDEConn().execute_all("SELECT * FROM dgmAttributeTypes")
        objs = [dgmAttributeTypes(**at) for at in ats]
        dgmAttributeTypes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def effects(self):
        dgmEffects.objects.all().delete()
        effects = SDEConn().execute_all("SELECT * FROM dgmEffects")
        objs = [dgmEffects(**effect) for effect in effects]
        dgmEffects.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def expressions(self):
        dgmExpressions.objects.all().delete()
        exps = SDEConn().execute_all("SELECT * FROM dgmExpressions")
        objs = [dgmExpressions(**exp) for exp in exps]
        dgmExpressions.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def type_attributes(self):
        dgmTypeAttributes.objects.all().delete()
        tas = SDEConn().execute_all("SELECT * FROM dgmTypeAttributes")
        objs = [dgmTypeAttributes(**ta) for ta in tas]
        dgmTypeAttributes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def type_effects(self):
        dgmTypeEffects.objects.all().delete()
        tes = SDEConn().execute_all("SELECT * FROM dgmTypeEffects")
        objs = [dgmTypeEffects(**te) for te in tes]
        dgmTypeEffects.objects.bulk_create(objs, batch_size=500)


class Eve:
    def all(self):
        self.graphics.delay(0)
        self.icons.delay(0)
        self.units.delay(0)
        return

    @staticmethod
    @shared_task
    def graphics(self):
        eveGraphics.objects.all().delete()
        egs = SDEConn().execute_all("SELECT * FROM eveGraphics")
        objs = [eveGraphics(**eg) for eg in egs]
        eveGraphics.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def icons(self):
        eveIcons.objects.all().delete()
        icns = SDEConn().execute_all("SELECT * FROM eveIcons")
        objs = [eveIcons(**icn) for icn in icns]
        eveIcons.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def units(self):
        eveUnits.objects.all().delete()
        units = SDEConn().execute_all("SELECT * FROM eveUnits")
        objs = [eveUnits(**unit) for unit in units]
        eveUnits.objects.bulk_create(objs, batch_size=500)


class Industry:
    def all(self):
        self.activity.delay(0)
        self.activity_materials.delay(0)
        self.activity_probabilities.delay(0)
        self.activity_products.delay(0)
        self.activity_races.delay(0)
        self.activity_skills.delay(0)
        self.blueprints.delay(0)
        return

    @staticmethod
    @shared_task
    def activity(self):
        industryActivity.objects.all().delete()
        acts = SDEConn().execute_all("SELECT * FROM industryActivity")
        objs = [industryActivity(**act) for act in acts]
        industryActivity.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def activity_materials(self):
        industryActivityMaterials.objects.all().delete()
        mats = SDEConn().execute_all("SELECT * FROM industryActivityMaterials")
        objs = [industryActivityMaterials(**mat) for mat in mats]
        industryActivityMaterials.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def activity_probabilities(self):
        industryActivityProbabilities.objects.all().delete()
        probs = SDEConn().execute_all("SELECT * FROM industryActivityProbabilities")
        objs = [industryActivityProbabilities(**prob) for prob in probs]
        industryActivityProbabilities.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def activity_products(self):
        industryActivityProducts.objects.all().delete()
        prods = SDEConn().execute_all("SELECT * FROM industryActivityProducts")
        objs = [industryActivityProducts(**prod) for prod in prods]
        industryActivityProducts.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def activity_races(self):
        industryActivityRaces.objects.all().delete()
        races = SDEConn().execute_all("SELECT * FROM industryActivityRaces")
        objs = [industryActivityRaces(**race) for race in races]
        industryActivityRaces.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def activity_skills(self):
        industryActivitySkills.objects.all().delete()
        skills = SDEConn().execute_all("SELECT * FROM industryActivitySkills")
        objs = [industryActivitySkills(**skill) for skill in skills]
        industryActivitySkills.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def blueprints(self):
        industryBlueprints.objects.all().delete()
        bps = SDEConn().execute_all("SELECT * FROM industryBlueprints")
        objs = [industryBlueprints(**bp) for bp in bps]
        industryBlueprints.objects.bulk_create(objs, batch_size=500)


class Inv:
    def all(self):
        self.categories.delay(0)
        self.contraband_types.delay(0)
        self.control_tower_resource_purposes.delay(0)
        self.control_tower_resources.delay(0)
        self.flags.delay(0)
        self.groups.delay(0)
        self.items.delay(0)
        self.market_groups.delay(0)
        self.meta_groups.delay(0)
        self.meta_types.delay(0)
        self.names.delay(0)
        self.positions.delay(0)
        self.traits.delay(0)
        self.type_materials.delay(0)
        self.type_reactions.delay(0)
        self.types.delay(0)
        self.unique_names.delay(0)
        self.volumes.delay(0)
        return

    @staticmethod
    @shared_task
    def categories(self):
        invCategories.objects.all().delete()
        cats = SDEConn().execute_all("SELECT * FROM invCategories")
        objs = [invCategories(**cat) for cat in cats]
        invCategories.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def contraband_types(self):
        invContrabandTypes.objects.all().delete()
        conts = SDEConn().execute_all("SELECT * FROM invContrabandTypes")
        objs = [invContrabandTypes(**cont) for cont in conts]
        invContrabandTypes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def control_tower_resource_purposes(self):
        invControlTowerResourcePurposes.objects.all().delete()
        cts = SDEConn().execute_all("SELECT * FROM invControlTowerResourcePurposes")
        objs = [invControlTowerResourcePurposes(**ct) for ct in cts]
        invControlTowerResourcePurposes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def control_tower_resources(self):
        invControlTowerResources.objects.all().delete()
        ctrs = SDEConn().execute_all("SELECT * FROM invControlTowerResources")
        objs = [invControlTowerResources(**ctr) for ctr in ctrs]
        invControlTowerResources.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def flags(self):
        invFlags.objects.all().delete()
        flags = SDEConn().execute_all("SELECT * FROM invFlags")
        objs = [invFlags(**flag) for flag in flags]
        invFlags.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def groups(self):
        invGroups.objects.all().delete()
        groups = SDEConn().execute_all("SELECT * FROM invGroups")
        objs = [invGroups(**group) for group in groups]
        invGroups.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def items(self):
        invItems.objects.all().delete()
        items = SDEConn().execute_all("SELECT * FROM invItems")
        objs = [invItems(**item) for item in items]
        invItems.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def market_groups(self):
        invMarketGroups.objects.all().delete()
        groups = SDEConn().execute_all("SELECT * FROM invMarketGroups")
        objs = [invMarketGroups(**group) for group in groups]
        invMarketGroups.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def meta_groups(self):
        invMarketGroups.objects.all().delete()
        groups = SDEConn().execute_all("SELECT * FROM invMetaGroups")
        objs = [invMetaGroups(**group) for group in groups]
        invMetaGroups.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def meta_types(self):
        invMetaTypes.objects.all().delete()
        metas = SDEConn().execute_all("SELECT * FROM invMetaTypes")
        objs = [invMetaTypes(**meta) for meta in metas]
        invMetaTypes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def names(self):
        invNames.objects.all().delete()
        names = SDEConn().execute_all("SELECT * FROM invNames")
        objs = [invNames(**name) for name in names]
        invNames.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def positions(self):
        invPositions.objects.all().delete()
        positions = SDEConn().execute_all("SELECT * FROM invPositions")
        objs = [invPositions(**position) for position in positions]
        invPositions.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def traits(self):
        invTraits.objects.all().delete()
        traits = SDEConn().execute_all("SELECT * FROM invTraits")
        objs = [invTraits(**trait) for trait in traits]
        invTraits.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def type_materials(self):
        invTypeMaterials.objects.all().delete()
        mats = SDEConn().execute_all("SELECT * FROM invTypeMaterials")
        objs = [invTypeMaterials(**mat) for mat in mats]
        invTypeMaterials.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def type_reactions(self):
        invTypeReactions.objects.all().delete()
        acts = SDEConn().execute_all("SELECT * FROM invTypeReactions")
        objs = [invTypeReactions(**act) for act in acts]
        invTypeReactions.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def types(self):
        invTypes.objects.all().delete()
        tps = SDEConn().execute_all("SELECT * FROM invTypes")
        objs = [invTypes(**tp) for tp in tps]
        invTypes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def unique_names(self):
        invUniqueNames.objects.all().delete()
        names = SDEConn().execute_all("SELECT * FROM invUniqueNames")
        objs = [invUniqueNames(**name) for name in names]
        invUniqueNames.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def volumes(self):
        invVolumes.objects.all().delete()
        volumes = SDEConn().execute_all("SELECT * FROM invVolumes")
        objs = [invVolumes(**volume) for volume in volumes]
        invVolumes.objects.bulk_create(objs, batch_size=500)


class Map:
    def all(self):
        self.solar_systems.delay(0)
        self.celestial_statistics.delay(0)
        self.constellation_jumps.delay(0)
        self.constellations.delay(0)
        self.denormalize.delay(0)
        self.jumps.delay(0)
        self.landmarks.delay(0)
        self.location_scenes.delay(0)
        self.location_wormhole_classes.delay(0)
        self.region_jumps.delay(0)
        self.regions.delay(0)
        self.solar_system_jumps.delay(0)
        self.universe.delay(0)
        return

    @staticmethod
    @shared_task
    def solar_systems(self):
        # Clear Old entries
        mapSolarSystems.objects.all().delete()

        # Create New Objects
        sde = SDEConn()
        systems = sde.execute_all("SELECT * FROM mapSolarSystems")
        objs = []
        for system in systems:
            obj = mapSolarSystems(**system)
            objs.append(obj)

        # Bulk Save
        mapSolarSystems.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def celestial_statistics(self):
        mapCelestialStatistics.objects.all().delete()
        stats = SDEConn().execute_all("SELECT * FROM mapCelestialStatistics")
        objs = [mapCelestialStatistics(**stat) for stat in stats]
        mapCelestialStatistics.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def constellation_jumps(self):
        mapConstellationJumps.objects.all().delete()
        jumps = SDEConn().execute_all("SELECT * FROM mapConstellationJumps")
        objs = [mapConstellationJumps(**jump) for jump in jumps]
        mapConstellationJumps.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def constellations(self):
        mapConstellations.objects.all().delete()
        consts = SDEConn().execute_all("SELECT * FROM mapConstellations")
        objs = [mapConstellations(**const) for const in consts]
        mapConstellations.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def denormalize(self):
        mapDenormalize.objects.all().delete()
        des = SDEConn().execute_all("SELECT * FROM mapDenormalize")
        objs = [mapDenormalize(**de) for de in des]
        mapDenormalize.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def jumps(self):
        mapJumps.objects.all().delete()
        jumps = SDEConn().execute_all("SELECT * FROM mapJumps")
        objs = [mapJumps(**jump) for jump in jumps]
        mapJumps.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def landmarks(self):
        mapLandmarks.objects.all().delete()
        marks = SDEConn().execute_all("SELECT * FROM mapLandmarks")
        objs = [mapLandmarks(**mark) for mark in marks]
        mapLandmarks.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def location_scenes(self):
        mapLocationScenes.objects.all().delete()
        scenes = SDEConn().execute_all("SELECT * FROM mapLocationScenes")
        objs = [mapLocationScenes(**scene) for scene in scenes]
        mapLocationScenes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def location_wormhole_classes(self):
        mapLocationWormholeClasses.objects.all().delete()
        whs = SDEConn().execute_all("SELECT * FROM mapLocationWormholeClasses")
        objs = [mapLocationWormholeClasses(**wh) for wh in whs]
        mapLocationWormholeClasses.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def region_jumps(self):
        mapRegionJumps.objects.all().delete()
        jumps = SDEConn().execute_all("SELECT * FROM mapRegionJumps")
        objs = [mapRegionJumps(**jump) for jump in jumps]
        mapRegionJumps.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def regions(self):
        mapRegions.objects.all().delete()
        regions = SDEConn().execute_all("SELECT * FROM mapRegions")
        objs = [mapRegions(**region) for region in regions]
        mapRegions.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def solar_system_jumps(self):
        mapSolarSystemJumps.objects.all().delete()
        jumps = SDEConn().execute_all("SELECT * FROM mapSolarSystemJumps")
        objs = [mapSolarSystemJumps(**jump) for jump in jumps]
        mapSolarSystemJumps.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def universe(self):
        mapUniverse.objects.all().delete()
        unis = SDEConn().execute_all("SELECT * FROM mapUniverse")
        objs = [mapUniverse(**uni) for uni in unis]
        mapUniverse.objects.bulk_create(objs, batch_size=500)


class Planet:
    def all(self):
        self.schematics.delay(0)
        self.schematics_pin_map.delay(0)
        self.schematics_type_map.delay(0)
        return

    @staticmethod
    @shared_task
    def schematics(self):
        planetSchematics.objects.all().delete()
        schems = SDEConn().execute_all("SELECT * FROM planetSchematics")
        objs = [planetSchematics(**schem) for schem in schems]
        planetSchematics.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def schematics_pin_map(self):
        planetSchematicsPinMap.objects.all().delete()
        schems = SDEConn().execute_all("SELECT * FROM planetSchematicsPinMap")
        objs = [planetSchematicsPinMap(**schem) for schem in schems]
        planetSchematicsPinMap.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def schematics_type_map(self):
        planetSchematicsTypeMap.objects.all().delete()
        schems = SDEConn().execute_all("SELECT * FROM planetSchematicsTypeMap")
        objs = [planetSchematicsTypeMap(**schem) for schem in schems]
        planetSchematicsTypeMap.objects.bulk_create(objs, batch_size=500)


class Ram:
    def all(self):
        self.activities.delay(0)
        self.assembly_line_stations.delay(0)
        self.assembly_line_type_detail_per_category.delay(0)
        self.assembly_line_type_detail_per_group.delay(0)
        self.assembly_line_types.delay(0)
        self.installation_type_contents.delay(0)
        return

    @staticmethod
    @shared_task
    def activities(self):
        ramActivities.objects.all().delete()
        acts = SDEConn().execute_all("SELECT * FROM ramActivities")
        objs = [ramActivities(**act) for act in acts]
        ramActivities.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def assembly_line_stations(self):
        ramAssemblyLineStations.objects.all().delete()
        lines = SDEConn().execute_all("SELECT * FROM ramAssemblyLineStations")
        objs = [ramAssemblyLineStations(**line) for line in lines]
        ramAssemblyLineStations.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def assembly_line_type_detail_per_category(self):
        ramAssemblyLineTypeDetailPerCategory.objects.all().delete()
        lines = SDEConn().execute_all("SELECT * FROM ramAssemblyLineTypeDetailPerCategory")
        objs = [ramAssemblyLineTypeDetailPerCategory(**line) for line in lines]
        ramAssemblyLineTypeDetailPerCategory.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def assembly_line_type_detail_per_group(self):
        ramAssemblyLineTypeDetailPerGroup.objects.all().delete()
        lines = SDEConn().execute_all("SELECT * FROM ramAssemblyLineTypeDetailPerGroup")
        objs = [ramAssemblyLineTypeDetailPerGroup(**line) for line in lines]
        ramAssemblyLineTypeDetailPerGroup.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def assembly_line_types(self):
        ramAssemblyLineTypes.objects.all().delete()
        lines = SDEConn().execute_all("SELECT * FROM ramAssemblyLineTypes")
        objs = [ramAssemblyLineTypes(**line) for line in lines]
        ramAssemblyLineTypes.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def installation_type_contents(self):
        ramInstallationTypeContents.objects.all().delete()
        contents = SDEConn().execute_all("SELECT * FROM ramInstallationTypeContents")
        objs = [ramInstallationTypeContents(**content) for content in contents]
        ramInstallationTypeContents.objects.bulk_create(objs, batch_size=500)


class Skin:
    def all(self):
        self.license.delay(0)
        self.materials.delay(0)
        self.skins.delay(0)
        self.ship.delay(0)
        return

    @staticmethod
    @shared_task
    def license(self):
        skinLicense.objects.all().delete()
        skins_ = SDEConn().execute_all("SELECT * FROM skinLicense")
        objs = [skinLicense(**skin) for skin in skins_]
        skinLicense.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def materials(self):
        skinMaterials.objects.all().delete()
        mats = SDEConn().execute_all("SELECT * FROM skinMaterials")
        objs = [skinMaterials(**mat) for mat in mats]
        skinMaterials.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def skins(self):
        skins.objects.all().delete()
        sks = SDEConn().execute_all("SELECT * FROM skins")
        objs = [skins(**sk) for sk in sks]
        skins.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def ship(self):
        skinShip.objects.all().delete()
        ships = SDEConn().execute_all("SELECT * FROM skinShip")
        objs = [skinShip(**ship) for ship in ships]
        skinShip.objects.bulk_create(objs, batch_size=500)


class Sta:
    def all(self):
        self.operations.delay(0)
        self.operations_services.delay(0)
        self.services.delay(0)
        self.stations.delay(0)
        self.station_types.delay(0)
        return

    @staticmethod
    @shared_task
    def operations(self):
        staOperations.objects.all().delete()
        ops = SDEConn().execute_all("SELECT * FROM staOperations")
        objs = [staOperations(**op) for op in ops]
        staOperations.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def operations_services(self):
        staOperationsServices.objects.all().delete()
        ops = SDEConn().execute_all("SELECT * FROM staOperationServices")
        objs = [staOperationsServices(**op) for op in ops]
        staOperationsServices.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def services(self):
        staServices.objects.all().delete()
        svcs = SDEConn().execute_all("SELECT * FROM staServices")
        objs = [staServices(**svc) for svc in svcs]
        staServices.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def stations(self):
        staStations.objects.all().delete()
        stations = SDEConn().execute_all("SELECT * FROM staStations")
        objs = [staStations(**station) for station in stations]
        staStations.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def station_types(self):
        staStationTypes.objects.all().delete()
        stations = SDEConn().execute_all("SELECT * FROM staStationTypes")
        objs = [staStationTypes(**station) for station in stations]
        staStationTypes.objects.bulk_create(objs, batch_size=500)


class Trn:
    def all(self):
        self.translation_tables.delay(0)
        self.translation_columns.delay(0)
        self.translation_languages.delay(0)
        self.translations.delay(0)
        return

    @staticmethod
    @shared_task
    def translation_tables(self):
        translationTables.objects.all().delete()
        tables = SDEConn().execute_all("SELECT * FROM translationTables")
        objs = [translationTables(**table) for table in tables]
        translationTables.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def translation_columns(self):
        trnTranslationColumns.objects.all().delete()
        columns = SDEConn().execute_all("SELECT * FROM trnTranslationColumns")
        objs = [trnTranslationColumns(**column) for column in columns]
        trnTranslationColumns.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def translation_languages(self):
        trnTranslationLanguages.objects.all().delete()
        langs = SDEConn().execute_all("SELECT * FROM trnTranslationLanguages")
        objs = [trnTranslationLanguages(**lang) for lang in langs]
        trnTranslationLanguages.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def translations(self):
        trnTranslations.objects.all().delete()
        trans = SDEConn().execute_all("SELECT * FROM trnTranslations")
        objs = [trnTranslations(**tran) for tran in trans]
        trnTranslations.objects.bulk_create(objs, batch_size=500)


class War:
    def all(self):
        self.combat_zones.delay(0)
        self.combat_zone_systems.delay(0)
        return

    @staticmethod
    @shared_task
    def combat_zones(self):
        warCombatZones.objects.all().delete()
        zones = SDEConn().execute_all("SELECT * FROM warCombatZones")
        objs = [warCombatZones(**zone) for zone in zones]
        warCombatZones.objects.bulk_create(objs, batch_size=500)

    @staticmethod
    @shared_task
    def combat_zone_systems(self):
        warCombatZoneSystems.objects.all().delete()
        zones = SDEConn().execute_all("SELECT * FROM warCombatZoneSystems")
        objs = [warCombatZoneSystems(**zone) for zone in zones]
        warCombatZoneSystems.objects.bulk_create(objs, batch_size=500)
